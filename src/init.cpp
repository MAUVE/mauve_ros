/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <ros/ros.h>
#include <mauve/ros/ros_init.hpp>

namespace mauve
{
  namespace ros
  {

bool configure(int thread_count) {
  if (::ros::master::check())
    ::ros::start();
  else {
    ::ros::shutdown();
    return false;
  }

  static ::ros::AsyncSpinner spinner(thread_count); // Use thread_count threads

  // TODO: Check spinner.canStart() to suppress errors / warnings once it's incorporated into ROS
  spinner.start();
  return true;
}

bool init_node(const std::string& node_name, int thread_count,
  const ::ros::M_string& remappings)
{
  ::ros::init(remappings, node_name, ::ros::init_options::AnonymousName);
  return configure(thread_count);
}

bool init_node(int& argc, char** argv, const std::string& node_name,
  int thread_count)
{
  ::ros::init(argc, argv, node_name, ::ros::init_options::AnonymousName);
  return configure(thread_count);
}

}} // namespaces
