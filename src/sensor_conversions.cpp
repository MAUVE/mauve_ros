/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/NavSatStatus.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Imu.h>

#include <mauve/types/sensor_types.hpp>
#include "mauve/ros/conversions.hpp"

namespace mauve {
  namespace ros {
    namespace conversions {

template<> bool convert(const sensor_msgs::LaserScan& a, types::sensor::LaserScan& b) {
  b.range_min = a.range_min;
  b.range_max = a.range_max;
  b.angle_min = a.angle_min;
  b.angle_max = a.angle_max;
  b.angle_increment = a.angle_increment;
  b.ranges = std::vector<double>(std::begin(a.ranges), std::end(a.ranges));
  return true;
}

template<> bool convert(const types::sensor::LaserScan& a, sensor_msgs::LaserScan& b) {
  b.range_min = a.range_min;
  b.range_max = a.range_max;
  b.angle_min = a.angle_min;
  b.angle_max = a.angle_max;
  b.angle_increment = a.angle_increment;
  b.ranges = std::vector<float>(std::begin(a.ranges), std::end(a.ranges));
  return true;
}

template<> bool convert(const sensor_msgs::NavSatStatus& a, types::sensor::GNSSStatus& b) {
  b.status = static_cast<types::sensor::FIX_STATUS>(a.status);
  b.service = static_cast<types::sensor::GNSS_SERVICE>(a.service);
  return true;
}

template<> bool convert(const types::sensor::GNSSStatus& a, sensor_msgs::NavSatStatus& b) {
  b.status = static_cast<int>(a.status);
  b.service = static_cast<int>(a.service);
  return true;
}

template<> bool convert(const sensor_msgs::NavSatFix& a, types::sensor::GNSSStatus& b) {
  return convert(a.status, b);
}

template<> bool convert(const types::sensor::GNSSStatus& a, sensor_msgs::NavSatFix& b) {
  return convert(a, b.status);
}

template<> bool convert(const sensor_msgs::Joy& a, types::sensor::Joy& b) {
  b.axes.clear();
  std::copy(a.axes.begin(), a.axes.end(), std::back_inserter(b.axes));
  b.buttons.clear();
  std::copy(a.buttons.begin(), a.buttons.end(), std::back_inserter(b.buttons));
  return true;
}

template<> bool convert(const types::sensor::Joy& a, sensor_msgs::Joy& b){
  b.axes.clear();
  std::copy(a.axes.begin(), a.axes.end(), std::back_inserter(b.axes));
  b.buttons.clear();
  std::copy(a.buttons.begin(), a.buttons.end(), std::back_inserter(b.buttons));
  return true;
}

template<>
bool convert(const types::sensor::Imu& mauve, sensor_msgs::Imu& ros)
{
  //logger().debug("IMU ROS conversion function");
  ros.header.seq++;
  //ros.header.stamp.sec = static_cast<uint32_t>(mauve.timestamp/1e9);
  //ros.header.stamp.nsec = static_cast<uint32_t>(mauve.timestamp-ros.header.stamp.sec);
  ros.header.frame_id = "xsens";
  ros.orientation.x = mauve.orientation.x;
  ros.orientation.y = mauve.orientation.y;
  ros.orientation.z = mauve.orientation.z;
  ros.orientation.w = mauve.orientation.w;
  //ros.orientation_covariance = ;
  ros.angular_velocity.x = mauve.angular_velocity.x;
  ros.angular_velocity.y = mauve.angular_velocity.y;
  ros.angular_velocity.z = mauve.angular_velocity.z;
  //ros.angular_velocity_covariance = ;
  ros.linear_acceleration.x = mauve.linear_acceleration.x;
  ros.linear_acceleration.y = mauve.linear_acceleration.y;
  ros.linear_acceleration.z = mauve.linear_acceleration.z;
  //ros.linear_acceleration_covariance = ;
  return true;
}

template<>
bool convert(const sensor_msgs::Imu& ros, types::sensor::Imu& mauve)
{
  mauve.timestamp = ros.header.stamp.sec*1e9 + ros.header.stamp.nsec;
  convert(ros.orientation, mauve.orientation);
  convert(ros.angular_velocity, mauve.angular_velocity);
  convert(ros.linear_acceleration, mauve.linear_acceleration);
  // convert magnetometer?
  return true;
}

    }
  }
}
