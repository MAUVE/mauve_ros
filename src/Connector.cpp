/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <ros/ros.h>
#include <mauve/ros/ros_init.hpp>
#include <mauve/ros/Connector.hpp>
#include <mauve/ros/Shell.hpp>

namespace mauve
{
  namespace ros
  {

::ros::NodeHandle* Connector::node = nullptr;
::ros::NodeHandle* Connector::node_private = nullptr;

bool Connector::init() {
  if (this->node == nullptr) {
    this->logger().info("Initializing ROS node");
    if (::ros::isInitialized() || init_node("mauve", 1)) {
      this->node = new ::ros::NodeHandle();
      this->node_private = new ::ros::NodeHandle("~");
    }
    else {
      this->logger().error("Unable to initialize ROS node");
      return false;
    }
  }
  return true;
}

bool RosAbstractShell::configure_hook() { return this->init(); }

}} // namespaces
