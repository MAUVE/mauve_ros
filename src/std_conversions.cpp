/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt64.h>

#include "mauve/ros/conversions.hpp"

namespace mauve {
  namespace ros {
    namespace conversions {

template<> bool convert(const bool& v, std_msgs::Bool& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Bool& r, bool& v) { v = r.data; return true; }

template<> bool convert(const float& v, std_msgs::Float32& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Float32& r, float& v) { v = r.data; return true; }

template<> bool convert(const double& v, std_msgs::Float32& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Float32& r, double& v) { v = r.data; return true; }

template<> bool convert(const double& v, std_msgs::Float64& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Float64& r, double& v) { v = r.data; return true; }

template<> bool convert(const short& v, std_msgs::Int16& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Int16& r, short& v) { v = r.data; return true; }

template<> bool convert(const long& v, std_msgs::Int32& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Int32& r, long& v) { v = r.data; return true; }

template<> bool convert(const long long& v, std_msgs::Int64& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Int64& r, long long& v) { v = r.data; return true; }

template<> bool convert(const int& v, std_msgs::Int16& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Int16& r, int& v) { v = r.data; return true; }

template<> bool convert(const int& v, std_msgs::Int32& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Int32& r, int& v) { v = r.data; return true; }

template<> bool convert(const int& v, std_msgs::Int64& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::Int64& r, int& v) { v = r.data; return true; }

template<> bool convert(const std::string& v, std_msgs::String& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::String& r, std::string& v) { v = r.data; return true; }

template<> bool convert(const unsigned short& v, std_msgs::UInt16& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::UInt16& r, unsigned short& v) { v = r.data; return true; }

template<> bool convert(const unsigned long& v, std_msgs::UInt32& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::UInt32& r, unsigned long& v) { v = r.data; return true; }

template<> bool convert(const unsigned long long& v, std_msgs::UInt64& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::UInt64& r, unsigned long long& v) { v = r.data; return true; }

template<> bool convert(const unsigned int& v, std_msgs::UInt16& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::UInt16& r, unsigned int& v) { v = r.data; return true; }

template<> bool convert(const unsigned int& v, std_msgs::UInt32& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::UInt32& r, unsigned int& v) { v = r.data; return true; }

template<> bool convert(const unsigned int& v, std_msgs::UInt64& r) { r.data = v; return true; }
template<> bool convert(const std_msgs::UInt64& r, unsigned int& v) { v = r.data; return true; }

    }
  }
}
