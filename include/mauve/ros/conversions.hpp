/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_CONVERSION_HPP
#define MAUVE_ROS_CONVERSION_HPP

namespace mauve {
  namespace ros {

    /**
     * Namespace containing conversion functions
     */
    namespace conversions {

      /**
       * The conversion function
       * \tparam T source type
       * \tparam U target type
       * @param t the source data to convert
       * @param u the target data
       * @return conversion success
       */
      template <typename T, typename U>
      bool convert(const T& t, U& u);

      /**
       * The identity conversion function
       * \tparam T type
       * @param t the source data to convert
       * @param u the target data
       * @return conversion success
       */
      template <typename T>
      bool convert(const T& t, T& u) { u = t; return true;};

    }
  }
}

#endif
