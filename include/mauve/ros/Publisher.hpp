/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_PUBLISHER_HPP
#define MAUVE_ROS_PUBLISHER_HPP

#include <string>
#include <functional>

#include <ros/ros.h>

#include <mauve/runtime.hpp>
#include <mauve/runtime/PeriodicStateMachine.hpp>

#include "Connector.hpp"
#include "Shell.hpp"

namespace mauve
{
  namespace ros
  {

    /**
     * Adapter of the ROS Publisher.
     * \tparam T MAUVE Type
     * \tparam ROS_T ROS Type
     */
    template <typename T, typename ROS_T>
    struct Publisher
      : public Connector
      , virtual public runtime::WithLogger
    {
    protected:
      /**
       * Shutdown the publisher
       */
      virtual void shutdown() final;
      /**
       * Open publisher on topic \a topic
       * \param topic The topic name
       */
      virtual bool open(const std::string& topic) final;
      /** The ROS publisher */
      ::ros::Publisher publisher;
      /** The ROS message owned by the resource */
      ROS_T msg;
      /** A mutex to protect the \a msg */
      runtime::RtMutex mutex;
    };

    /**
     * Core of the ROS Publisher Resource.
     * \tparam T MAUVE Type
     * \tparam ROS_T ROS Type
     */
    template <typename T, typename ROS_T>
    struct PublisherCore
      : public runtime::Core< RosShell<T, ROS_T> >
      , public Publisher<T, ROS_T>
    {
      virtual bool configure_hook() override {
        return this->open(this->shell().topic);
      }
      virtual void cleanup_hook() override {
        return this->shutdown();
      }
      /** Publish value to ROS topic.
       * @param value MAUVE value to publish.
       */
      void publish(T value);
      /** Update function: reads input and publish. */
      void update();

    };

    /**
     * Interface of the ROS Publisher Resource.
     * \tparam T MAUVE Type
     * \tparam ROS_T ROS Type
     */
    template <typename T, typename ROS_T>
    struct PublisherResourceInterface
    : public runtime::Interface< RosShell<T, ROS_T>,
        PublisherCore<T, ROS_T> >
    {
      /** The write service to write a data into the resource */
      runtime::WriteService<T> & write = this->template mk_write_service<T>("write", &PublisherCore<T, ROS_T>::publish);
    };

    template <typename T, typename ROS_T>
    using PublisherResource = runtime::Resource<
      RosShell<T, ROS_T>,
      PublisherCore<T, ROS_T>,
      PublisherResourceInterface<T, ROS_T> >;

    template <typename T, typename ROS_T>
    using PublisherComponent = runtime::Component<
      RosShell<T, ROS_T>,
      PublisherCore<T, ROS_T>,
      runtime::PeriodicStateMachine<RosShell<T, ROS_T>, PublisherCore<T, ROS_T>>>;

}} // namespaces

#include "ipp/Publisher.ipp"

#endif
