/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_SUBSCRIBER_IPP
#define MAUVE_ROS_SUBSCRIBER_IPP

#include "../Subscriber.hpp"

namespace mauve
{
  namespace ros
  {

    // ========================= Core =========================
    template <typename ROS_T, typename T>
    bool SubscriberCore<ROS_T, T>::configure_hook() {
      this->status = runtime::DataStatus::NO_DATA;

      auto topic = this->shell().topic.get_value();

      this->logger().info("Subscribing to topic {}", topic);
      if (topic.empty()) return false;
      if(topic.at(0) == '~')
          subscriber = this->shell().node_private->subscribe(topic.substr(1), 10, &SubscriberCore<ROS_T, T>::callback, this);
        else
          subscriber = this->shell().node->subscribe(topic, 10, &SubscriberCore<ROS_T, T>::callback, this);
      return true;
    }

    template <typename ROS_T, typename T>
    void SubscriberCore<ROS_T, T>::cleanup_hook() {
      subscriber.shutdown();
    }

    template <typename ROS_T, typename T>
    void SubscriberCore<ROS_T, T>::callback(const ROS_T& msg)
    {
      mutex.lock();
      auto conv = this->shell().conversion.get_value();
      this->logger().debug("New data {}", msg);
      if (conv(msg, this->value))
        this->status = runtime::DataStatus::NEW_DATA;
      else
        this->logger().warn("Conversion failed for {}", msg);
      mutex.unlock();
    }

    template <typename ROS_T, typename T>
    void SubscriberCore<ROS_T, T>::update() {
      mutex.lock();
      if (this->status == runtime::DataStatus::NEW_DATA) {
        this->shell().write_port.write(this->value);
        this->status = runtime::DataStatus::OLD_DATA;
        //this->logger().debug("received {}", this->value);
      }
      mutex.unlock();
    };

  } // namespace ros
} // namespace mauve

#endif
