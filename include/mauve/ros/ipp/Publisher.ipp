/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_PUBLISHER_IPP
#define MAUVE_ROS_PUBLISHER_IPP

#include "../Publisher.hpp"
#include "../conversions.hpp"

namespace mauve
{
  namespace ros
  {

    template <typename T, typename ROS_T>
    bool Publisher<T, ROS_T>::open(const std::string& topic) {
      if (topic.empty()) return false;
      if(topic.at(0) == '~')
        publisher = node_private->advertise<ROS_T>(topic.substr(1), 1);
      else
        publisher = node->advertise<ROS_T>(topic, 1);
      return true;
    }

    template <typename T, typename ROS_T>
    void Publisher<T, ROS_T>::shutdown() {
      publisher.shutdown();
    }

    template <typename T, typename ROS_T>
    void PublisherCore<T, ROS_T>::publish(T value)
    {
      auto conv = this->shell().conversion.get_value();
      this->mutex.lock();
      if (conv(value, this->msg))
        this->publisher.publish(this->msg);
      this->mutex.unlock();
    }

    template <typename T, typename ROS_T>
    void PublisherCore<T, ROS_T>::update()
    {
      auto in = this->shell().read_port.read();
      if (in.status == runtime::DataStatus::NEW_DATA)
        this->publish(in.value);
    }

  } // namespace ros
} // namespace mauve

#endif
