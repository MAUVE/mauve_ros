/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ROS_SUBSCRIBER_HPP
#define MAUVE_ROS_SUBSCRIBER_HPP

#include <string>
#include <functional>

#include <ros/ros.h>

#include <mauve/runtime.hpp>
#include <mauve/runtime/PeriodicStateMachine.hpp>

#include "Connector.hpp"
#include "Shell.hpp"

namespace mauve
{
  namespace ros
  {

    /**
     * Core of the ROS Subscriber.
     * \tparam T MAUVE Type
     * \tparam ROS_T ROS Type
     */
    template <typename ROS_T, typename T>
    struct SubscriberCore : public runtime::Core< RosShell<ROS_T, T> >
    {
      bool configure_hook() final override;
      void cleanup_hook() final override;
      /**
       * Read the value received on the topic.
       * \return the received value converted to type T
       */
      inline T read_value() {
        mutex.lock();
        if (status == runtime::DataStatus::NEW_DATA)
          status = runtime::DataStatus::OLD_DATA;
        mutex.unlock();
        return value;
      };
      /**
       * Read the value received on the topic with its status.
       * \return the received value converted to type T with s tatus
       */
      inline runtime::StatusValue<T> read() {
        mutex.lock();
        auto sv = runtime::StatusValue<T> {status, value};
        if (status == runtime::DataStatus::NEW_DATA)
          status = runtime::DataStatus::OLD_DATA;
        mutex.unlock();
        return sv;
      };
      /**
       * Read the status of the value received on the topic.
       * \return the status of the received value
       */
      inline runtime::DataStatus read_status() { return status; };
      /** Update function: write received message to output. */
      void update();

    private:
      /** The subscriber callback */
      void callback(const ROS_T& msg);
      /** The ROS subscriber */
      ::ros::Subscriber subscriber;
      /** The value owned by the resource */
      T value;
      /** The status of the value */
      runtime::DataStatus status;
      /** A mutex to protect the \a value */
      runtime::RtMutex mutex;
    };

    /**
     * Interface of the ROS Subscriber.
     * \tparam T MAUVE Type
     * \tparam ROS_T ROS Type
     */
    template <typename ROS_T, typename T>
    struct SubscriberInterface :
      public runtime::Interface< RosShell<ROS_T, T>, SubscriberCore<ROS_T, T> >
    {
      /** The read service to read the data owned by the resource */
      runtime::ReadService<runtime::StatusValue<T>> & read = this->template mk_read_service<runtime::StatusValue<T>>("read", &SubscriberCore<ROS_T, T>::read);
      runtime::ReadService<T> & read_value = this->template mk_read_service<T>("read_value", &SubscriberCore<ROS_T, T>::read_value);
      runtime::ReadService<runtime::DataStatus> & read_status = this->template mk_read_service<runtime::DataStatus>("read_status", &SubscriberCore<ROS_T, T>::read_status);
    };

    template <typename ROS_T, typename T>
    using SubscriberResource = runtime::Resource< RosShell<ROS_T, T>,
      SubscriberCore<ROS_T, T>, SubscriberInterface<ROS_T, T> >;

    template <typename ROS_T, typename T>
    using SubscriberComponent = runtime::Component< RosShell<ROS_T, T>,
        SubscriberCore<ROS_T, T>,
        runtime::PeriodicStateMachine<RosShell<ROS_T, T>,
          SubscriberCore<ROS_T, T>> >;

}} // namespaces

#include "ipp/Subscriber.ipp"

#endif
