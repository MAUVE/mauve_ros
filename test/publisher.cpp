/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>
#include <std_msgs/String.h>

#include <mauve/ros/conversions.hpp>

using namespace mauve::runtime;
using namespace mauve::ros;

struct MyShell : public Shell {
  WritePort<std::string> & data = mk_write_port<std::string>("data");
};

struct MyCore : public Core<MyShell> {
  void update() {
    shell().data.write("hello");
  }
};

using MyComponent = Component<MyShell, MyCore, PeriodicStateMachine<MyShell, MyCore> >;

struct TestArchitecture : public Architecture {
  using pub_t = PublisherResource<std::string,std_msgs::String>;

  pub_t & pub_convert = mk_resource<pub_t>("pub_convert");
  pub_t & pub_conversion = mk_resource<pub_t>("pub_conversion", "/my_str");
  pub_t & pub_construct = mk_resource<pub_t>("pub_construct", "/my_str",
    conversions::convert<std::string,std_msgs::String>);

  MyComponent & c1 = mk_component<MyComponent>("cpt_convert");
  MyComponent & c2 = mk_component<MyComponent>("cpt_conversion");
  MyComponent & c3 = mk_component<MyComponent>("cpt_construct");

  PublisherComponent<std::string, std_msgs::String> & pub_comp =
    mk_component<PublisherComponent<std::string, std_msgs::String>>("publisher",
      conversions::convert<std::string, std_msgs::String>
    );

  SharedData<std::string> & message =
    mk_resource<SharedData<std::string>>("message", "hello world");

  virtual bool configure_hook() override {
    c1.shell().data.connect(pub_convert.interface().write);
    c2.shell().data.connect(pub_conversion.interface().write);
    c3.shell().data.connect(pub_construct.interface().write);

    pub_convert.shell().topic = "/my_str";
    pub_convert.shell().conversion.set_value([this](const std::string& data, std_msgs::String& msg) -> bool {
      if (data == "hello") {
        msg.data = "world";
        return true;
      } else return false;
    });
    pub_conversion.shell().conversion.set_value(conversions::convert<std::string,std_msgs::String>);

    pub_comp.shell().topic = "/my_str";
    pub_comp.fsm().period = sec_to_ns(1);

    pub_comp.shell().read_port.connect(message.interface().read);

    return Architecture::configure_hook();
  }
};

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: debug" << std::endl;
  AbstractLogger::initialize(config);

  auto archi = new TestArchitecture();
  auto depl = mk_deployer(archi);
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  archi->message.core().write("hello you");

  depl->loop();

  depl->stop();
  archi->cleanup();
  return 0;
}
