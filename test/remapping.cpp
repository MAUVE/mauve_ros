/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE ROS project.
 *
 * MAUVE ROS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ROS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <mauve/runtime.hpp>
#include <mauve/ros.hpp>
#include <std_msgs/String.h>

#include <mauve/ros/conversions.hpp>

using namespace mauve::runtime;
using namespace mauve::ros;

struct MyShell : public Shell {
  WritePort<std::string> & data = mk_write_port<std::string>("data");
};

struct MyCore : public Core<MyShell> {
  void update() {
    shell().data.write("hello");
  }
};

using MyComponent = Component<MyShell, MyCore, PeriodicStateMachine<MyShell, MyCore> >;

struct TestArchitecture : public Architecture {
  using pub_t = PublisherResource<std::string,std_msgs::String>;

  pub_t & pub = mk_resource<pub_t>("pub_construct", "my_str",
    conversions::convert<std::string,std_msgs::String>);

  MyComponent & cpt = mk_component<MyComponent>("cpt");

  virtual bool configure_hook() override {
    cpt.shell().data.connect(pub.interface().write);

    return Architecture::configure_hook();
  }
};

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: debug" << std::endl;
  AbstractLogger::initialize(config);

  std::map<std::string, std::string> remap = { {"my_str", "mauve/str"} };
  init_node("mauve", 1, remap);

  auto archi = new TestArchitecture();
  auto depl = mk_deployer(archi);
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  archi->cleanup();
  return 0;
}
